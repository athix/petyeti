#!/usr/bin/env python3

from inky import InkyPHAT
import time
from PIL import Image
import os

inky_display = InkyPHAT("red")

def main():

    image_location = "/home/pi/petyeti/backgrounds"
    file_list = os.listdir(image_location)
    
    for image in file_list:

        img = Image.open(image_location + '/' + image)
        inky_display.set_image(img)
        inky_display.show()

        starttime = time.time()
        time.sleep(5.0 - ((time.time() - starttime) % 5.0))

    exit()

if __name__ == '__main__':
    main()
