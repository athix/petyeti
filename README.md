# PetYeti

Home for all the main sourcecode, installers, and etc for the PetYeti "personal pet yeti" AI driven toy that will run on a RPiZW with an e-ink screen.

We will be posting images of mockups (and prototypes) as well as design ideas and psudo-code that will help us refine and clarify the initial implementation of the gadget and software.  
We have (mostly) nailed down the crucial starter-point and have the prototype to a beta level, so we have taken this project public and invite any/all commers to participate with PRs etc.
Please remember this warning in your checkins though - **do NOT _ever_ include passowrds or auth tokens in your code.**  Do not store hardcoded passwords or private keys in the project either.  Anything pushed to the repository will evenntually be facing the public view - so be warned.
